﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using MySql.Data.MySqlClient;
using Dapper;
using WebApi.Models;

namespace WebApi.DAL
{
    public class ATT_MASTER_GROUP_DAL
    {
        private string strConn = "";
        public ATT_MASTER_GROUP_DAL()
        {
            strConn = ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString;
        }

        // select
        public IEnumerable<ATT_MASTER_GROUP> GetAll()
        {
            using(MySqlConnection conn = new MySqlConnection(strConn))
            {
                string query = @"SELECT GROUP_ID, GROUP_CODE, GROUP_DESC, GROUP_STATUS
                                   FROM ATT_MASTER_GROUP 
                                  ORDER BY GROUP_ID;";

                var result = conn.Query<ATT_MASTER_GROUP>(query);
                return result;
            }
        }

        
    }
}