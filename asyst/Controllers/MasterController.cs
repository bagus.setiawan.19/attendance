﻿using asyst.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using asyst.DAL.Master;
using asyst.Models.MasterGroup;

namespace asyst.Controllers
{
    public class MasterController : Controller
    {
        public ActionResult Station()
        {
            var data = new MenuDAL();
            return View("Station", data.navbarItems().ToList());
        }

        public ActionResult Group()
        {
            var data = new MenuDAL();
            return View("Group", data.navbarItems().ToList());
        }

        public JsonResult getDataMasterGroup()
        {
            MasterGroupDAL dal = new MasterGroupDAL();
            var result = dal.GetAll();
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Employee()
        {
            var data = new MenuDAL();
            return View("Employee", data.navbarItems().ToList());
        }

        public JsonResult AddDataMasterGroup(ATT_MASTER_GROUP data)
        {
            MasterGroupDAL dal = new MasterGroupDAL();
            var result = dal.AddData(data);
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }
    }
}
