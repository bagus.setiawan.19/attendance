﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using asyst.DAL;

namespace asyst.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var data = new MenuDAL();
            return View("Index", data.navbarItems().ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            var data = new MenuDAL();
            return View("About", data.navbarItems().ToList());
            //return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}