﻿using asyst.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace asyst.DAL
{
    public class MenuDAL
    {
        public IEnumerable<Sidebar> navbarItems()
        {
            var menu = new List<Sidebar>();
            menu.Add(new Sidebar { Id = 1, nameOption = "Home", controller = "Home", action = "Index", imageClass = "fa fa-home fa-fw", status = true, isParent = false, parentId = 0 });

            menu.Add(new Sidebar { Id = 2, nameOption = "Master", imageClass = "fa fa-wrench fa-fw", status = true, isParent = true, parentId = 0 });
            menu.Add(new Sidebar { Id = 3, nameOption = "Master Station", controller = "Master", action = "Station", status = true, isParent = false, parentId = 2 });
            menu.Add(new Sidebar { Id = 4, nameOption = "Master Group", controller = "Master", action = "Group", status = true, isParent = false, parentId = 2 });
            menu.Add(new Sidebar { Id = 5, nameOption = "Master Employee", controller = "Master", action = "Employee", status = true, isParent = false, parentId = 2 });

            menu.Add(new Sidebar { Id = 6, nameOption = "Monitoring", imageClass = "fa fa-desktop fa-fw", status = true, isParent = true, parentId = 0 });
            menu.Add(new Sidebar { Id = 7, nameOption = "Monitoring Attendance", controller = "Monitoring", action = "Attendance", status = true, isParent = false, parentId = 6 });

            //menu.Add(new Sidebar { Id = 5, nameOption = "Tables", controller = "Home", action = "Tables", imageClass = "fa fa-table fa-fw", status = true, isParent = false, parentId = 0 });
            //menu.Add(new Sidebar { Id = 6, nameOption = "Forms", controller = "Home", action = "Forms", imageClass = "fa fa-edit fa-fw", status = true, isParent = false, parentId = 0 });
            //menu.Add(new Sidebar { Id = 7, nameOption = "UI Elements", imageClass = "fa fa-wrench fa-fw", status = true, isParent = true, parentId = 0 });
            //menu.Add(new Sidebar { Id = 8, nameOption = "Panels and Wells", controller = "Home", action = "Panels", status = true, isParent = false, parentId = 7 });
            //menu.Add(new Sidebar { Id = 9, nameOption = "Buttons", controller = "Home", action = "Buttons", status = true, isParent = false, parentId = 7 });
            //menu.Add(new Sidebar { Id = 10, nameOption = "Notifications", controller = "Home", action = "Notifications", status = true, isParent = false, parentId = 7 });
            //menu.Add(new Sidebar { Id = 11, nameOption = "Typography", controller = "Home", action = "Typography", status = true, isParent = false, parentId = 7 });
            //menu.Add(new Sidebar { Id = 12, nameOption = "Icons", controller = "Home", action = "Icons", status = true, isParent = false, parentId = 7 });
            //menu.Add(new Sidebar { Id = 13, nameOption = "Grid", controller = "Home", action = "Grid", status = true, isParent = false, parentId = 7 });
            //menu.Add(new Sidebar { Id = 14, nameOption = "Multi-Level Dropdown", imageClass = "fa fa-sitemap fa-fw", status = true, isParent = true, parentId = 0 });
            //menu.Add(new Sidebar { Id = 15, nameOption = "Fisrt Level Item", status = true, isParent = false, parentId = 14 });
            //menu.Add(new Sidebar { Id = 19, nameOption = "Second Level Item", status = true, isParent = true, parentId = 14 });
            //menu.Add(new Sidebar { Id = 20, nameOption = "Third Level Item", status = true, isParent = false, parentId = 19 });
            //menu.Add(new Sidebar { Id = 16, nameOption = "Sample Pages", imageClass = "fa fa-files-o fa-fw", status = true, isParent = true, parentId = 0 });
            //menu.Add(new Sidebar { Id = 17, nameOption = "Blank Page", controller = "Home", action = "Blank", status = true, isParent = false, parentId = 16 });
            //menu.Add(new Sidebar { Id = 18, nameOption = "Login Page", controller = "Home", action = "Login", status = true, isParent = false, parentId = 16 });
            menu.Add(new Sidebar { Id = 21, nameOption = "About", controller = "Home", action = "About", imageClass = "fa fa-map-marker fa-fw", status = true, isParent = false, parentId = 0 });

            return menu.ToList();
        }
    }
}