﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Configuration;
using MySql.Data.MySqlClient;
using Dapper;
using asyst.Models.MasterGroup;
using asyst.Models;

namespace asyst.DAL.Master
{
    public class MasterGroupDAL
    {
        private string strConn = "";

        public MasterGroupDAL()
        {
            strConn = ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString;
        }

        // Get All Data
        public IEnumerable<ATT_MASTER_GROUP> GetAll()
        {
            using (MySqlConnection conn = new MySqlConnection(strConn))
            {
                string query = @"SELECT GROUP_ID, GROUP_CODE, GROUP_DESC, GROUP_STATUS
                                   FROM ATT_MASTER_GROUP 
                                  ORDER BY GROUP_ID;";

                var result = conn.Query<ATT_MASTER_GROUP>(query);
                return result;
            }
        }

        //public OutputModel Edit(string)
        //{
        //    string sts = "";
        //    string msg = "";
        //    using (MySqlConnection conn = new MySqlConnection(strConn))
        //    {
        //        string query = @"UPDATE ATT_MASTER_GROUP
        //                            SET GROUP_CODE = :GROUP_CODE,
        //                                GROUP_DESC = :GROUP_DESC,
        //                                GROUP_STATUS = :GROUP_STAT
        //                          WHERE GROUP_ID = :GROUP_ID";
        //        var result = conn.Execute(query)
        //    }
        //}

        public OutputModel AddData(ATT_MASTER_GROUP data)
        {
            string sts = "";
            string msg = "";
            using (MySqlConnection conn = new MySqlConnection(strConn))
            {
                string query = @"INSERT INTO ATT_MASTER_GROUP(GROUP_ID, GROUP_CODE, GROUP_DESC, GROUP_STATUS) 
                                VALUES(@GROUP_ID, @GROUP_CODE, @GROUP_DESC, @GROUP_STATUS)";
                try
                {
                    var param = new
                    {
                        Id = data.GROUP_ID,
                        Code = data.GROUP_CODE,
                        Desc = data.GROUP_DESC,
                        Status = data.GROUP_STATUS
                    };
                    conn.Execute(query, param);
                    sts = "S";
                    msg = "Data berhasil disimpan.";
                }
                catch (MySqlException ex)
                {
                    //throw new Exception(ex.Message);
                    sts = "E";
                    msg = ex.Message;
                }
                OutputModel result = new OutputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }
    }
}